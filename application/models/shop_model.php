<?php

class Shop_model extends CI_Model 
{

	public function insert_shop($data)
	{
		$this->db->insert('shops',$data);
		return ($this->db->affected_rows() != 1 ) ? false:true;
	}
	public function getcout_shoplist()
	{
		
       return $this->db->from('shops')->count_all_results();
	}
	public function get_shoplist($per_page,$search_data,$page)
	{
		$this->db->select('*');
		$this->db->from('shops');
		//$this->db->where('status',1);
		

        if ($search_data != '') {

            $this->db->like('shop_name', $search_data);
            $this->db->or_like('shop_address', $search_data);
            $this->db->or_like('shop_email', $search_data);
            $this->db->or_like('shop_mobile', $search_data);
        
        }
		
		   if (!isset($page)) {
				$page = 1;
			} 
			$page = ($page-1) * $per_page; 

		
		 $this->db->limit($per_page, $page);
        $this->db->order_by("shop_id", "desc");
		$query =$this->db->get();
		return $query->result_array();
	}
		public function get_shoplistpdf()
	{
		$this->db->select('*');
		$this->db->from('shops');
		//$this->db->where('status',1);
		

		
	
		$query =$this->db->get();
		return $query->result_array();
	}
	public function delete_shop($id,$data)
	{
		
		$this->db->where('shop_id', $id);
        $this->db->delete('shops');
		return ($this->db->affected_rows() != 1 ) ? false:true;
	}
	public function edit_shop($id)
	{
		$this->db->select('*');
		$this->db->from('shops');
		$this->db->where('shop_id',$id);
		
		$query =$this->db->get();
		return $query->row_array();

	}
	public function update_shop($data,$id)
	{
		$this->db->where('shop_id',$id);
		$this->db->update('shops',$data);
		return ($this->db->affected_rows() != 1 ) ? false:true;
	}
}
