<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_crud extends CI_Controller
{

   	function __construct()
	{
		parent::__construct();
		
		$this->load->model('shop_model');
		
		
	}	
	
	
    public function index() {

        $this->load->library('pagination');
        $config['per_page'] = 6;

    
        //$tempdb = clone $this->db;
        //$total_row = $tempdb->from('shops')->count_all_results();
		
		$total_row =$this->shop_model->getcout_shoplist();
       
        //$result['shop_list'] = $this->db->get('shops')->result_array();
		    $page = $this->input->get('page', true);
        $search_data = $this->input->get('search_data', true);
		$result['shop_list']=$this->shop_model->get_shoplist($config['per_page'],$search_data,$page);

        $config['base_url'] = site_url()."?search_data=$search_data";
        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';


        //Optional (Pagination Design)
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '&lt;Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;</i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';


        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();


        $data['content'] = $this->load->view('shop_list', $result, true);
        $this->load->view('master', $data);
    }

    public function create() {


        if (isset($_POST['shop_name'])) {
            $post_data = $_POST;
          
			
			$result = $this->shop_model->insert_shop($post_data);
			
            if ($result) {
                $this->session->set_flashdata('message', 'Succefully Created Student Info.');
            } else {
                $this->session->set_flashdata('message', "An error occurred while inserting data.");
            }
            redirect('shop_crud');
        }
    }

    public function view($shop_id) {
        if ($shop_id != '') {
			
			$row['shop_info'] = $this->shop_model->edit_shop($shop_id);
           
        }
        $data['content'] = $this->load->view('view', $row, true);
        $this->load->view('master', $data);
    }

    public function update($shop_id) {

        if (isset($_POST['shop_id'])) {
            $update_data = $_POST;
            $shop_id = $update_data['shop_id'];
         
			$result = $this->shop_model->update_shop($update_data,$shop_id);

            if ($result) {

                $this->session->set_flashdata('message', 'Succefully Updated Shop Info.');
            } else {
                $this->session->set_flashdata('message', 'An error occurred while inserting data');
            }

            //  redirect('shop');
        }
        if ($shop_id != '') {
            $this->db->where('shop_id', $shop_id);
            $row['shop_info'] = $this->db->get('shops')->row_array();
        }
        $data['content'] = $this->load->view('update', $row, true);
        $this->load->view('master', $data);
    }

    public function delete($shop_id) {

        if ($shop_id != '') {
            $this->db->where('shop_id', $shop_id);
            $result = $this->db->delete('shops');
            if ($result) {
                $this->session->set_flashdata('message', 'Succefully Deleted Shop Info.');
            } else {
                $this->session->set_flashdata('message', "An error occurred while inserting data.");
            }
            redirect('shop_crud');
        }
    }
	
	
//To Generate  the Pdf Report
	public function pdf()
	{
		//$this->load->library('mpdf');
		
		  //$this->load->library('pdf');
		    $this->load->library('Pdf');
    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', true);
    $pdf->SetTitle('Robin Interview ');
	$pdf->SetDefaultMonospacedFont('freeserif');
    $pdf->SetHeaderMargin(0);
    $pdf->SetTopMargin(1);
    $pdf->setFooterMargin(20);
    $pdf->SetAutoPageBreak(true);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);



	 $pdf->AddPage();
	 
     
	 $result['shop_list']=$this->shop_model->get_shoplistpdf();
	
	 
	 $htmlfirst=$this->load->view('pdf', $result, true);
	 
	 $pdf->writeHTML($htmlfirst,true, true, true, true, true);
	 ob_end_clean();
 $pdf->Output('pdfexample.pdf', 'I');
 
 


		//$data['student']=$this->student_model->get_student();
		//$this->load->view('pdf',$data);
	}

	

}
