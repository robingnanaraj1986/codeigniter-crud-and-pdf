<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <title>Store List</title>
</head>
<body>

 <h2 align="center">Store List</h2>
  <table border="1" align="center" cellpadding="1" cellspacing="0">
<thead>
    <tr>
        <td><b>S.No.</b></td>
        <td><b>Shop Name </b></td>
        <td><b>Address </b></td>
        <td><b>Email</b></td>
        <td><b>Phone</b></td>
    </tr>
</thead>
<tbody> 
    <?php $count = 1;
   if(isset($shop_list)){
                    foreach ($shop_list as $key => $value) {?>
        <tr>
            <td><?php echo $count; ?></td>
                             <td><?php echo  $value['shop_name'];?></td>
                    <td><?php echo  $value['shop_address'];?></td>
                    <td><?php echo  $value['shop_email'];?></td>
                    <td><?php echo  $value['shop_mobile'];?></td>
        </tr>
   <?php $count++; } } ?>
</tbody>
</table>

</body>
</html>