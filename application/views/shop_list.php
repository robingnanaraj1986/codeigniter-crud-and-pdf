
 <div class="row ">
        <div class="col-lg-2">
          <h3>Shop List</h3>   
        </div>
</div>
		
 <div class="row ">
  
        <div class="col-lg-8 mt-12" >
            <form action="" method="get">
        
                            <table width="100%">
							  <tr>
							   <td>
                                <input type="text" value="<?php if(isset($_GET['search_input'])){echo $_GET['search_data'];} ?>"  id="search_data" name="search_data"  class="form-control" placeholder="Search" />
                              </td>
							  <td>
							  
                                    <button class="button button-green" type="submit">
                                        Search
                                    </button>
							  </td>
                            </tr>
                           </table>							
                               
                          
            
            
        </form>
                    
       </div>
          <div class="col-lg-2 ">
              <button  class="button button-purple mt-12 pull-right" data-toggle="modal" data-target="#create_modal"> Create Shop</button> 
      
          </div>
		  
		  <div class="col-lg-2 ">
              <a  class="button button-purple mt-12 pull-right" style="color:#FFF" href="<?php echo site_url('pdf_shop')?>" target="_blank"> Pdf </a> 
      
          </div>
    </div>
         <?php 
        
        if($this->session->flashdata('message')){
                echo "<p class='custom-alert'>".$this->session->flashdata('message');"</p>";
        // unset($_SESSION['message']);
       }
     
        ?>
<table class="table mt-12">
            <thead>
                <tr>
             
                    <th>Shop Name</th>
					<th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>
                
          
                <?php

                if(isset($shop_list)){
                    foreach ($shop_list as $key => $value) {
                        
                ?>

                <tr >
                   
                    <td><?php echo  $value['shop_id'];?> <?php echo  $value['shop_name'];?></td>
                    <td><?php echo  $value['shop_address'];?></td>
                    <td><?php echo  $value['shop_email'];?></td>
                    <td><?php echo  $value['shop_mobile'];?></td>
                    
                 
                <td class="text-right">
                 
                    <a href="<?php echo site_url('delete_shop').'/'.$value['shop_id'];?>"    class="button button-red" >Delete</a> 
                  
                    <a href="<?php echo site_url('update_shop').'/'.$value['shop_id'];?>"   class="button button-blue" >Edit</a> 
                  
                    <a  href="<?php echo site_url('view_shop').'/'.$value['shop_id'];?>"   class="button button-green" >View</a> 

                    
                
                </td>
                    
                    
                    
                </tr>
                
                <?php
                
                    }
                }
                ?>
                

           </tbody>
        </table>
    

<div class="pull-right">
 
   <?php if(isset($pagination)) {
       echo $pagination;
                
        }
        ?>
   
</div>

 <div class="modal fade" id="create_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Shop</h4>
        </div>
        <div class="modal-body">
         
            <form method="post"  id="create_frm" action="<?php echo site_url('create_shop'); ?>" >
            <div class="form-group">
                <label for="shop_name">Shop Name:</label>
                <input type="text" name="shop_name" id="shop_name" class="form-control" required maxlength="50">
            </div>
        
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="shop_address" id="shop_address"  maxlength="50">
            </div>
			
			<div class="form-group">
                <label for="email_address">Email:</label>
                <input type="email" class="form-control" name="shop_email" id="shop_email" required maxlength="50">
            </div>
			
			<div class="form-group">
                <label for="email_address">Mobile:</label>
                <input type="text" class="form-control" name="shop_mobile" id="shop_mobile" required maxlength="50">
            </div>
			
			

                <div class="form-group mb-50">
            <input type="submit" class="button button-green  pull-right"  value="Submit"/>
                </div>
                
        </form> 
    
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> 