-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2020 at 12:07 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codetest`
--

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `shop_id` int(11) NOT NULL,
  `shop_name` varchar(100) NOT NULL,
  `shop_address` varchar(255) NOT NULL,
  `shop_email` varchar(100) NOT NULL,
  `shop_mobile` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`shop_id`, `shop_name`, `shop_address`, `shop_email`, `shop_mobile`, `datetime`) VALUES
(15, 'mobile Shop', 'shop, 1st floor', 'mobile@mail.com', '9007655', '2020-04-29 13:44:08'),
(16, 'Rice shop', 'jeeva, kk dist', '97655321@gmail.com', '09065555', '2020-04-29 13:44:43'),
(17, 'MilkShop', '110, Kerala', '34534532@gmail.com', '90076543', '2017-08-24 09:44:21'),
(14, 'Lap Store', 'adreess, India', 'mail@gg.com', '900766', '2017-08-24 09:44:21'),
(18, 'Veg shop', 'attingal, TVM', 'rolobalet@gamil.com', '07373633124', '2020-05-02 02:33:01'),
(19, 'Medical shop', 'medi, india', 'medi@mailss.com', '9005565676', '2020-05-02 02:48:43'),
(20, 'Ice shop', 'attingal, road-TVM', 'robincodet@gamil.com', '07373633124', '2020-05-02 02:50:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`shop_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
